const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');
const app = express();
const nodemailer = require('nodemailer');
const deeplink = require('node-deeplink');
// enable files upload
app.use(fileUpload({
    createParentPath: true
}));

//add other middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev'));

//start app 
const port = process.env.PORT || 3000;

app.listen(port, () => 
  console.log(`App is listening on port ${port}.`)
);

app.post('/upload', async (req, res) => {
    try {
        if(!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
			console.log("ddd")
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let avatar = req['files']['file'];
            console.log(avatar)
            //Use the mv() method to place the file in upload directory (i.e. "uploads")
            avatar.mv('./uploads/' + avatar.name);

            //send response
            res.send({
                status: true,
                message: 'File is uploaded',
                data: {
                    name: avatar.name,
                    mimetype: avatar.mimetype,
                    size: avatar.size
                }
            });
        }
    } catch (err) {
		console.log(err)
        res.status(500).send(err);
    }
});
app.get('/', async (req, res) => {
var transporter = nodemailer.createTransport({
    
	    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
     auth: {
       user: 'hamidanidal@gmail.com',
       pass: 'nidallfiingisamm'
     }
});

var mailOptions = {
     from: 'hamidanidal@gmail.com',
     to: 'ooredoofieldservice@gmail.com',
     subject: 'Sending Email using Node.js',
     html: "<h1>bonjour</h1><a href='myapp://coachFootball.com'>Bonjour</a>"
};

transporter.sendMail(mailOptions, function(error, info){
     if (error) {
       console.log(error);
     } else {
       console.log('Email sent: ' + info.response);
     }
});
 res.send({
                status: true,
                message: 'File is uploaded',
              
            });
});

app.get(
  '/restpassword', async (req, res) => {
	  console.log(req["query"]["id"])
  deeplink({
	  url: 'myapp://coachFootball.com',
    fallback: 'http://coachFootball.com',
    android_package_name: 'coach.Football',
   
  })
  });