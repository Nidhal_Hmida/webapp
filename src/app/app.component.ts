import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular'
import { Pages } from './interfaces/pages';
import { GlobalsService } from './services/globals/globals.service';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { Facebook } from '@ionic-native/facebook/ngx';
import { CoachfootballService } from './services/coachfootballservice.service';
import { TranslationService } from './services/translation.service';
import { User } from './models/user.model';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { timer } from 'rxjs/Observable/timer';
import { ProgramGroup } from './models/programgroup.model';
registerLocaleData(localeFr);

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  appPages: Array<Pages>;
  user : User
  selectedLanguage:string; 
  programgroups1 : ProgramGroup [] = [{"id":"1","title":"Tirs","photo":"../../../assets/img/penaltie.jpg"},{"id":"2","title":"Passes","photo":"../../../assets/img/passes.jpg"},{"id":"3","title":"Vitesse","photo":"../../../assets/img/vitesse.jpg"},{"id":"4","title":"Dribbles","photo":"../../../assets/img/dribble.jpg"},
  {"id":"5","title":"Coordination","photo":"../../../assets/img/coordination.jpg"},{"id":"6","title":"Juggling","photo":"../../../assets/img/jj.jpg"},{"id":"7","title":"physique","photo":"../../../assets/img/physique.jpg"},{"id":"8","title":"Panna","photo":"../../../assets/img/panna.jpg"}
 ]
  constructor(
    private platform: Platform,
    public navCtrl: NavController,
    public global : GlobalsService,
    private fb: Facebook,
    private service :CoachfootballService,
 
  ) 
  {
    this.appPages = [
      {
        title: "Plans d'entrainement",
        url: 'programs',
        direct: 'forward',
        icon: 'body'
      },

      {
        title: 'Personaliser entrainement',
        url: '/personalizetraining',
        direct: 'forward',
        icon: 'football-outline'
      },
      {
        title: 'Vidéos de challenges',
        url: '/videochallengelist',
        direct: 'forward',
        icon: 'logo-youtube'
      }, 
      {
        title: 'Profil',
        url: '/edit-profile',
        direct: 'forward',
        icon: 'person-circle'
      },
      {
        title: "Conditions d'utilisation",
        url: '/rules',
        direct: 'forward',
        icon: 'document-outline'
      },
      {
        title: 'Payement',
        url: '/payement',
        direct: 'forward',
        icon: 'cash-outline'
      },
      {
        title: 'Déconnexion',
        url: '/login',
        direct: 'forward',
        icon: 'log-out-outline'
      }
    ];
   
       /* this.user =   await   this.global.getUserProfil()*/
   
    /*if(this.user== null)
    {
      this.navCtrl.navigateRoot("/login")
       }*/
       this.getProgramsgroup()
  }
    
      getProgramsgroup()
      {
         if(this.service.programsgroups.length ==0)
        {
         this.service.get("/program_groups").subscribe((data: ProgramGroup [])=>{  
          this.image(data)
          this.service.serviceProgrammsGroup(data) 
          timer(5000).subscribe(()=>{
            this.global.setShowSplash(false)
         
          })
       
             });
        }
      
      }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  
  logout() {
    this.navCtrl.navigateRoot('/login');
  }

  logoutFacebook() {
    this.fb.logout()
  }

  image(data)
  {
    for(var i=0;i<data.length;i++)
    {
      data[i]["photo"] = this.programgroups1[i]["photo"] 
    }
  }
}
