export class Seance
{
    id : string
    title : string
    description : string
    photo : string
    exercicesNumber : number
    totalDuration : number
   
}