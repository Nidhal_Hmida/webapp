import { Exercise } from './exercice.model'

export class  Training{
   
    id? : string
    exercise : Exercise
   /* idUser? : string*/
    startTime : string
    opinion? : string
    duration? : number

    constructor(exercice :Exercise,dateDebut : string,id? : string,avis? : string,duration? :number)
    {
      this.id = id
      this.exercise = exercice
   /*   this.idUser = idCompte*/
      this.startTime = dateDebut
      this.opinion = avis 
      this.duration = duration
    }

}