import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { User } from 'src/app/models/user.model';


@Injectable({
  providedIn: 'root'
})
export class GlobalsService {
  userProfil  : User
  today: string | null = new DatePipe("FR-US").transform(new Date(), "yyyy-MM-dd");
  showSplash :boolean = true

  
    async getUserProfil() : Promise<User>
   {
    
   await this.nativeStorage.getItem('userProfil')
    .then(
     ( data => {this.userProfil=data;}),
      error => alert(error+"ddd")
    );
    return(this.userProfil)
    }

   setUserProfil(user :any)
   {
    this.nativeStorage.setItem('userProfil', user)
    .then(
      () => {},
      error => alert(error)
    );
  
  }

  getShowSplash()
  {
    return(this.showSplash)
  }
 
  setShowSplash(show)
  {
    this.showSplash = show
  }
  constructor(private nativeStorage: NativeStorage) { }
}
