import { Injectable } from '@angular/core';
import { Exercise } from '../models/exercice.model';
import { ProgramGroup } from '../models/programgroup.model';
import { Seance } from '../models/seance.model';
import { Training } from '../models/training.model';
import { User } from '../models/user.model';
import { VideoChallenge } from '../models/videochallenge.model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';
import { Program } from '../models/program.model';
@Injectable({
  providedIn: 'root'
})
export class CoachfootballService {

   optionRequete = {
    headers: new HttpHeaders({ 
      'Access-Control-Allow-Origin': '*',
      'Acces-Control-Expose-Headers':'*',
      'Acces-Control-Allow-Methods' : '*'
    })
  };
  //PROVISOIRE
  users : User[]
  user : User
  trainings : Training[]=[]
  currentTraining : Training
  programsgroups : ProgramGroup[]=[]
  programs : Program[]=[]
  currentProgram : Program
  seances : Seance[]
  currentSeance : Seance
  exercices : Exercise[]  = []
  currentExercice : Exercise
  video : VideoChallenge [] = []
  createSeance : boolean
  id : string
  path:string ="http://vps597745.ovh.net/footBallCoachApi/public/api"
  constructor(private httpClient: HttpClient) { }

  
  get(endPoint)
  {

    return this.httpClient.get(this.path+endPoint,this.optionRequete).pipe(shareReplay(1));
  }

  getChild(endPoint,id,children)
  {

    return this.httpClient.get(this.path+endPoint+"/"+id+"/"+children,this.optionRequete).pipe(shareReplay(1));
  }

  
  addTrainning(endPoint,training : Training)
  {
    
    return this.httpClient.post(this.path+"/"+endPoint,training,this.optionRequete).pipe(shareReplay(1)); 
  }
  
  modifyTraining(endPoint,training : Training)
  {
    
    return this.httpClient.put(this.path+"/"+endPoint+"/"+training.id,training,this.optionRequete).pipe(shareReplay(1)); 
  }

  deleteTraining(endPoint,training : Training)
  {
    return this.httpClient.delete(this.path+"/"+endPoint+"/"+training.id,this.optionRequete).pipe(shareReplay(1)); 













    .0
  }

  
  

  getData(data,table)
  {
    for(var i =0 ; i<table.length ; i++)
    {
      if(data["id"]== table[i]["id"])
      return i
    }
    return -1
  }

  singnUp(user : User )
  {
    var users = []
    if(JSON.parse(localStorage.getItem("users"))!= undefined)
    users = JSON.parse(localStorage.getItem("users"))
    users.push(user)
    localStorage.setItem("users",JSON.stringify(users))
    console.log(users)
    return true
  }

  singnIn(user : User )
  {
    var users = []
    if(JSON.parse(localStorage.getItem("users"))!= undefined)
    users = JSON.parse(localStorage.getItem("users"))
    console.log(users)
    for(var i =0 ; i<users.length ; i++)
    {
      if(user.email == users[i]["email"] && user.password== users[i]["password"])
      return users[i]
    } 
    return null
  }

  addVideo()
  {
    var videos = [{id :'1234',idCompte :'1234', seance : 'Séance tir au but',socialMedia: true, title : 'Validation séance 1 penaltie' ,  path : 'https://www.youtube.com//embed/OFXBMoKSEyc'},{id :'1234',idCompte :'1235', seance : 'Séance tir au but ',socialMedia: true, title : 'Validation séance 2 cornaire' ,  path : 'https://www.youtube.com//embed/x9jYSoWclPY',  badge :  "badge niveau 1",note : "16"},{id :'1236',idCompte :'1234', seance : 'Séance tir au but ',socialMedia: true, title : 'Validation séance 2 cornaire' ,  path : 'https://www.youtube.com//embed/x9jYSoWclPY',  badge :  "badge niveau 2",note : "14"}]   
    localStorage.setItem("videos",JSON.stringify(videos))
  }

  


  updateUser(user :User)
  {
    var users = []
    if(JSON.parse(localStorage.getItem("users"))!= undefined)
    users = JSON.parse(localStorage.getItem("users"))
    var indice = this.getData(user,users) 
    users.splice(indice,1)
    users.splice(indice,0,user)
  }


  serviceTraining(data)
  {
    this.trainings= data
  }

  setCurrentTraining(data)
  {
    this.currentTraining = data
  }

  serviceExercices(data)
  {
    this.exercices= data
  }

  setCurrentExercice(data)
  {
    this.currentExercice = data
  }

  serviceProgrammsGroup(data)
  {
    this.programsgroups= data
  }

  serviceSeance(data)
  {
    this.seances= data
  }

  setCurrentSeance(data)
  {
    this.currentSeance = data
  }

  servicePrograms(data)
  {
    this.programs= data
  }

  setCurrentProgram(data)
  {
    this.currentProgram = data
  }


  serviceVideos(data)
  {
    this.serviceVideos= data
  }

  serviceId(data)
  {
    this.id = data
  }

  serviceUser(data)
  {
    this.user = data
  }
  

  

 
}
