import { NgModule } from '@angular/core';
import { RankingPageRoutingModule } from './payement-routing.module';
import {  PayementPage } from './payement.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
   SharedModule,
    RankingPageRoutingModule
  ],
  declarations: [ PayementPage]
})
export class PayementPageModule {}
