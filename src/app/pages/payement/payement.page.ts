import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController, MenuController, AlertController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';

@Component({
  selector: 'app-payement',
  templateUrl: './payement.page.html',
  styleUrls: ['./payement.page.scss'],
})
export class PayementPage implements OnInit {

  @ViewChild('content',{static : false}) content
  @ViewChild('content2',{static : false}) content2
  @ViewChild('content3',{static : false}) content3
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private renderer: Renderer2,
    private alertController: AlertController,
    private formBuilder: FormBuilder,
    private service :CoachfootballService,
    public global : GlobalsService 
  ) { }

  ngOnInit() {
  
  }

  back()
  {
    this.navCtrl.navigateBack('programs')
  }
  ionViewWillEnter()
  {
    this.menuCtrl.enable(true)
    this.renderer.setAttribute(this.content.el,"style", "height :40px;width:100%;color : white; border-top: 1px solid #fff;border-bottom: none")
    this.renderer.setAttribute(this.content2.el,"style", "height :40px;width:100%;color : white; border-top: 1px solid #fff;border-bottom: none")
    this.renderer.setAttribute(this.content3.el,"style", "height :40px;width:100%;color : white; border-top: 1px solid #fff;border-bottom: 1px solid #fff;")
  }

  pay(content)
  {
    if(content =='content')
    {
    this.renderer.setAttribute(this.content.el,"style", "height :40px;width:100%;background-color:white; border-top: 1px solid #fff;border-bottom: none;color:#2880e4")
    this.renderer.setAttribute(this.content2.el,"style", "height :40px;color : white; border-top: 1px solid #fff;border-bottom: none")
    this.renderer.setAttribute(this.content3.el,"style", "height :40px;color : white; border-top: 1px solid #fff;border-bottom: 1px solid #fff;")  
  }
    else if(content =='content2')
    {
      this.renderer.setAttribute(this.content.el,"style", "height :40px;width:100%;color : white; border-top: 1px solid #fff;border-bottom: none")  
    this.renderer.setAttribute(this.content2.el,"style", "height :40px;width:100%;background-color:white; border-top: 1px solid #fff;border-bottom: none;color:#2880e4")
    this.renderer.setAttribute(this.content3.el,"style", "height :40px;width:100%;color : white; border-top: 1px solid #fff;border-bottom: 1px solid #fff;")  
  
  }
    else
    {
      this.renderer.setAttribute(this.content.el,"style", "height :40px;width:100%;color : white; border-top: 1px solid #fff;border-bottom: none")
      this.renderer.setAttribute(this.content2.el,"style", "height :40px;width:100%;color : white; border-top: 1px solid #fff;border-bottom: none")  
    this.renderer.setAttribute(this.content3.el,"style", "height :40px;width:100%;background-color:white; border-top: 1px solid #fff;border-bottom: 1px solid #fff;color:#2880e4")
    }
  }

}
