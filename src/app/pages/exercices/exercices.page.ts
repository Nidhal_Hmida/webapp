import { Component } from '@angular/core';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Exercise } from 'src/app/models/exercice.model';

@Component({
  selector: 'app-exercices',
  templateUrl: './exercices.page.html',
  styleUrls: ['./exercices.page.scss'],
})
export class ExercicesPage  {
     exercices1 : any = [{id: '1234',title :'Tire au but',description :'entainement facile pour les débutants' , url :'https://www.youtube.com/embed/3qKOG6wbxbw',image:'../../../assets/img/panna.jpg',Trainings:[]},
     {id :'4567', title :'Coup franc',description :'entainement facile pour les débutants' , url :'https://www.youtube.com//embed/OFXBMoKSEyc',image:'../../../assets/img/passes.jpg',Trainings:[]},
     {id :'4756',title :'Geste technique',description :'entainement facile pour les débutants' , url :'https://www.youtube.com//embed/x9jYSoWclPY',image:'../../../assets/img/dribble.jpg',Trainings:[]},
     {id :'7456',title :'Cornaire',description :'entainement facile pour les débutants' , url :'https://www.youtube.com//embed/jDgJJTjmbf4',image:'../../../assets/img/physique.jpg',Trainings:[]}
 ]
     user :User
     exercices : Exercise[]
    
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    private service :CoachfootballService,
    public global : GlobalsService,
    private _sanitizationService: DomSanitizer
  ) { 
    
   }

   ionViewWillEnter()
   {
    this.menuCtrl.enable(false);
     this.exercices = this.service.exercices
     this.image()
   }

  image()
   {
     for(var i=0;i<this.exercices1.length;i++)
     {
       this.exercices[i]["photo"] = this.exercices1[i]["image"] 
       this.exercices[i]["url"] = this.exercices1[i]["url"]
     }
   }

  details(exercice : Exercise) {
    this.service.setCurrentExercice(exercice)
    this.navCtrl.navigateRoot("detailsexercice")
    }
  
    back()
    {
      this.navCtrl.navigateBack("detailsseance")
    }
  
}
