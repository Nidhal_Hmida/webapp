import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmailrestpasswordPage } from './emailrestpassword.page';

describe('EmailrestpasswordPage', () => {
  let component: EmailrestpasswordPage;
  let fixture: ComponentFixture<EmailrestpasswordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailrestpasswordPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmailrestpasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
