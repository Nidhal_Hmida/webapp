import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController, ActionSheetController, AlertController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
@Component({
  selector: 'app-emailrestpassword',
  templateUrl: './emailrestpassword.page.html',
  styleUrls: ['./emailrestpassword.page.scss'],
})
export class EmailrestpasswordPage implements OnInit {
 
  public onForm: FormGroup;
  constructor(  private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private service :CoachfootballService,) { 
     
  }

  ngOnInit(): void {
    this.onForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required,
        Validators.email
      ])] 
    });
  }

  forgotPass() {
    if(this.onForm.valid)
    {
    this.navCtrl.navigateForward("/resetpassword")
    }
    else
    {
     this.presentAlertConfirm()
    }
  }
  back()
  {
    this.navCtrl.navigateBack('login')
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Adresse email non valide',
      message: "<b>Ton adresse e-mail n'est pas valide <br> <br>Essaye de vérifier ton adresse e-mail et réessaye<br></b>",
      buttons: [
 {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }

}
