import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmailrestpasswordPage } from './emailrestpassword.page';

const routes: Routes = [
  {
    path: '',
    component: EmailrestpasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmailrestpasswordPageRoutingModule {}
