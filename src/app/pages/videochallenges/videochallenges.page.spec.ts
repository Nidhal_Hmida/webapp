import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VideochallengesPage } from './videochallenges.page';

describe('VideochallengesPage', () => {
  let component: VideochallengesPage;
  let fixture: ComponentFixture<VideochallengesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideochallengesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VideochallengesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
