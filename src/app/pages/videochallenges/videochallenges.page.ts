import { Component, OnInit} from '@angular/core';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File} from '@ionic-native/file/ngx';
import { FileUploadOptions, FileTransferObject,FileTransfer } from '@ionic-native/file-transfer/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';


 /*const MAX_FILE_SIZE = 5 * 1024 * 1024;*/
const ALLOWED_MIME_TYPE = "video/mp4";
const baseUrl = "http://192.168.1.13:3000";


@Component({
  selector: 'app-videochallenges',
  templateUrl: './videochallenges.page.html',
  styleUrls: ['./videochallenges.page.scss'],
})
export class VideochallengesPage implements OnInit {

  mediaFiles = []; 
  socialMedia : string
  title : string
  currentSeance : string ="tir au but"
  selectedVideo: string = ''
  uploadedVideo: string;
  isUploading: boolean = false;
  uploadPercent: number = 0;
  videoFileUpload: FileTransferObject;
  loader;

  constructor(private mediaCapture: MediaCapture,
              private menuCntrl : MenuController,
              public navCtrl: NavController,
              public menuCtrl: MenuController,
              public loadingCtrl: LoadingController,
              private camera: Camera,
              private file: File,
              private transfer : FileTransfer,
              private streamingMedia: StreamingMedia,
              private toastController: ToastController,
              private alertController: AlertController,
              private service :CoachfootballService) {   }

          

  async viewSelectedVideo(dirpath,filename)
{
  try {
    var dirUrl = await this.file.resolveDirectoryUrl(dirpath);
    var retrievedFile = await this.file.getFile(dirUrl, filename, {});
  } catch(err) {
   
    alert('probléme de téléchargement vidéo')
  }
  
  retrievedFile.file( data => {
  
  /*   if (data.size > MAX_FILE_SIZE)  alert( "You cannot upload more than 5mb.");*/
      if (data.type !== ALLOWED_MIME_TYPE) alert( "Incorrect file type.");
      this.selectedVideo = retrievedFile.nativeURL
      let options: StreamingVideoOptions = {
        successCallback: () => {  },
        errorCallback: (e) => { alert('Error streaming') },
        orientation: 'portrait',
        shouldAutoClose: true,
        controls: true
      };    
      this.streamingMedia.playVideo(this.selectedVideo, options);       
  });
}
 
recordVideo() {
  let options: CaptureVideoOptions = { quality : 100 }
  this.mediaCapture.captureVideo(options).then(
   (data: MediaFile[]) => {
    this.viewSelectedVideo(data[0]["fullPath"].substr(0, data[0]["fullPath"].lastIndexOf('/') + 1),data[0]["name"])
    },
    (err: CaptureError) => alert("Probléme de capture vidéo")
  );
}
  

  selectVideo() {
    const options: CameraOptions = {
      mediaType: this.camera.MediaType.VIDEO,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    }

    this.camera.getPicture(options)
      .then(  (videoUrl) => {
        if (videoUrl) {
          var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
          var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);       
          dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
           this.viewSelectedVideo(dirpath,filename)   
        }
      },
      (err) => {
      alert(err)
      });
  }

  uploadVideo() {
    var url = baseUrl +  "/upload";
    var options: FileUploadOptions = {
      fileName: this.title+".mp4",
      fileKey: "file",
      mimeType: "video/mp4"
    }
    this.videoFileUpload = this.transfer.create();
    this.isUploading = true;
    this.videoFileUpload.upload(this.selectedVideo, url, options)
      .then((data)=>{
        this.isUploading = false;
        this.uploadPercent = 0;
        return JSON.parse(data.response);
      })
      .then((data) => {        
        this.uploadedVideo = data.url;
        alert( "Video upload was successful.");
      })
      .catch((err)=>{
        this.isUploading = false;
        this.uploadPercent = 0;
        alert("Error uploading video.");
      });

    this.videoFileUpload.onProgress((data) => {
      this.uploadPercent = Math.round((data.loaded/data.total) * 100);
      this.selectedVideo= ''
      this.title=''
    });
  }

  ngOnInit() {
    this.menuCntrl.enable(false)    
  }

  back()
  {
    this.navCtrl.navigateBack("videochallenges")
  }

  async showLoader() {
    this.loader = this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
    });
    await this.loader.present();
  }

  dismissLoader() {
    this.loader.dismiss();
  }

  cancelSelection() {
    this.selectedVideo= '';
  }

  cancelUpload() {
    this.videoFileUpload.abort();
    this.uploadPercent = 0;
  }  
}
