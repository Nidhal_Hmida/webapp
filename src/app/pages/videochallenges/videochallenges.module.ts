import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideochallengesPageRoutingModule } from './videochallenges-routing.module';

import { VideochallengesPage } from './videochallenges.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    VideochallengesPageRoutingModule
  ],
  declarations: [VideochallengesPage]
})
export class VideochallengesPageModule {}
