import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideochallengesPage } from './videochallenges.page';

const routes: Routes = [
  {
    path: '',
    component: VideochallengesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideochallengesPageRoutingModule {}
