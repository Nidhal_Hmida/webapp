import { NgModule } from '@angular/core';
import { PersonalizetrainingPageRoutingModule } from './personalizetraining-routing.module';
import { PersonalizetrainingPage } from './personalizetraining.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    PersonalizetrainingPageRoutingModule
  ],
  declarations: [PersonalizetrainingPage        
  ]
})
export class PersonalizetrainingPageModule {}
