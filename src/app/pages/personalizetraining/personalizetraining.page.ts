import { Component, EventEmitter, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MenuController, NavController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
  selector: 'app-personalizetraining',
  templateUrl: './personalizetraining.page.html',
  styleUrls: ['./personalizetraining.page.scss'],
})
export class PersonalizetrainingPage implements OnInit {
    
  step =1
  myDate:string;
  age 
  height : number = 175
  weight : number = 80
  position : string 
  onForm: FormGroup;
  @ViewChild('content',{static : false}) content
  
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private datePicker: DatePicker
  ) { 

  }

  ngOnInit() {

    this.onForm = this.formBuilder.group({
      'age': [null, Validators.compose([
        Validators.required,
      ])],
        'weight': [null,Validators.compose([
        Validators.required,
      ])],
      'length': [null,Validators.compose([
        Validators.required,       
      ])],
      'position': [null,Validators.compose([
        Validators.required,       
      ])],
    });
    this.menuCtrl.enable(true)
    this.getAge(new Date(1990,5,12))
  }

  
  back() {
     
    this.step--
    if(this.step ==1)
    this.renderer.setAttribute(this.content.el,"style", "--ion-background-color:#000 ")
  
  }
 
  next() {
    this.step++
    this.renderer.setAttribute(this.content.el,"style", "--ion-background-color:#2880e4; ")
}


increment () {
  if(this.weight<200)
  this.weight++;
}

decrement () {
  if(this.weight>30)
  this.weight--;

} 

increment1 () {
  if(this.height<= 220)
  this.height++;
}

decrement1 () {
  if(this.height>=120)
  this.height--;

} 

setPosition(pos)
{
  this.position  = pos
}

getAge(date) { 
 var diff = Date.now() - date.getTime();
  var age1 = new Date(diff); 
  this.age=Math.abs(age1.getUTCFullYear() - 1970);
}

showDatepicker(){
  this.datePicker.show({
    date: new Date(),
    mode: 'date',
    androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
    okText:"Valider",
    todayText:"Aujourd'hui"
  }).then(
    date => {
      
      this.getAge(new Date(date.getFullYear(),date.getMonth(),date.getDate()))
      this.myDate = date.getDate()+"/"+date.toLocaleString('default', { month: 'long' })+"/"+date.getFullYear();
    }
  );
}  

ionViewDidLeave()
{
  this.step =1
  this.renderer.setAttribute(this.content.el,"style", "--ion-background-color:#000 ")
}



}
