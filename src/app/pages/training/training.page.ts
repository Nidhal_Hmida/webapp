import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, AlertController, LoadingController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';
import { Training } from 'src/app/models/training.model';

@Component({
  selector: 'app-training',
  templateUrl: './training.page.html',
  styleUrls: ['./training.page.scss'],
})

export class TrainingPage implements OnInit {
  trainings : Training[]
   
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public service :CoachfootballService,
    public global : GlobalsService,
  ) { 
   
  }

  ionViewWillEnter()
  {
    this.trainings = this.service.trainings
  }
  
  back()
  {
  this.navCtrl.navigateRoot("detailsexercice")
  }
  
  details(training) {
      this.service.setCurrentTraining(training)
      this.navCtrl.navigateRoot("detailstraining")
    }


  ngOnInit() {
  this.menuCtrl.enable(false)
  }
}





