import { Component, OnInit } from '@angular/core';
import { VideoChallenge } from '../../models/videochallenge.model';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-videochallengelist',
  templateUrl: './videochallengelist.page.html',
  styleUrls: ['./videochallengelist.page.scss'],
})
export class VideochallengelistPage implements OnInit {

  videos : VideoChallenge[]
  exercices : any = [{id: '1234',title :'Tire au but',description :'entainement facile pour les débutants' , url :'https://www.youtube.com/embed/3qKOG6wbxbw',Trainings:[]},
  {id :'4567', title :'Coup franc',description :'entainement facile pour les débutants' , url :'https://www.youtube.com//embed/OFXBMoKSEyc',Trainings:[]},
  {id :'4756',title :'Geste technique',description :'entainement facile pour les débutants' , url :'https://www.youtube.com//embed/x9jYSoWclPY',Trainings:[]},
  {id :'7456',title :'Cornaire',description :'entainement facile pour les débutants' , url :'https://www.youtube.com//embed/jDgJJTjmbf4',Trainings:[]}
]
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    private service :CoachfootballService,
    public global : GlobalsService,
    private _sanitizationService: DomSanitizer
  ) { }

  ionViewWillEnter() {
    this.service.addVideo()
    this.menuCtrl.enable(true);
      if(this.service.video.length == 0)
      {
        this.videos = JSON.parse(localStorage.getItem("videos"))
        this.service.serviceVideos(this.videos)
      }
      else
      this.videos = this.service.video
  }

  security(src)
  {
    return this._sanitizationService.bypassSecurityTrustResourceUrl(src);
  }

  add()
  {
    this.navCtrl.navigateForward("videochallenges")
  }
  ngOnInit() {
  }

}
