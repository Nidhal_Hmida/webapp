import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VideochallengelistPage } from './videochallengelist.page';

describe('VideochallengelistPage', () => {
  let component: VideochallengelistPage;
  let fixture: ComponentFixture<VideochallengelistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideochallengelistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VideochallengelistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
