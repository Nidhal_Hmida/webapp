import { NgModule } from '@angular/core';
import { VideochallengelistPageRoutingModule } from './videochallengelist-routing.module';
import { VideochallengelistPage } from './videochallengelist.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    VideochallengelistPageRoutingModule
  ],
  declarations: [VideochallengelistPage]
})
export class VideochallengelistPageModule {}
