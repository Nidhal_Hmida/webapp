import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsexercicePageRoutingModule } from './detailsexercice-routing.module';

import { DetailsexercicePage } from './detailsexercice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsexercicePageRoutingModule
  ],
  declarations: [DetailsexercicePage]
})
export class DetailsexercicePageModule {}
