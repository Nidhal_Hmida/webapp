import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsexercicePage } from './detailsexercice.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsexercicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsexercicePageRoutingModule {}
