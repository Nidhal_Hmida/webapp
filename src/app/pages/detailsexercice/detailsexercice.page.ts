import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Training } from 'src/app/models/training.model'
import { Exercise } from 'src/app/models/exercice.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-detailsexercice',
  templateUrl: './detailsexercice.page.html',
  styleUrls: ['./detailsexercice.page.scss'],
})
export class DetailsexercicePage implements OnInit {

  exercice : Exercise
  affiche: boolean=null;
   
  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    public service :CoachfootballService,
    public global : GlobalsService,
    private _sanitizationService: DomSanitizer) {
     
     }

   
    ionViewWillEnter()
    { 
      this.exercice = this.service.currentExercice
      this.service .getChild("/exercises",this.exercice.id,"trainings").subscribe((data)=>{  
        this.service.serviceTraining(data)  
        if(this.service.trainings.length>0)
        {
          this.verifyLast()
        }
        else
        this.affiche = true
      });    
    }

    verifyLast()
    {
      if(this.service.trainings[this.service.trainings.length-1].duration==85)
      this.affiche = false
      else
      this.affiche = true
    }


  
  ngOnInit() {
    this.exercice = this.service.currentExercice    
  }

  tempsEcoule(date1,date2)
  {
    var ms_Min = 60 * 1000; 
    var dateStart :any  = date1.replace(' ', 'T')
    var dateEnd : any = date2.replace(' ', 'T')
    dateStart  = new Date(date1);
    dateEnd  = new Date(date2);
    var diff = dateEnd - dateStart; 
    return Math.round(diff / ms_Min) ;         
}

  
  async createTraining()
  {
    var training = new Training(new Exercise(this.exercice.id), new DatePipe("fr-FR").transform(Date.now(),'yyyy-MM-dd HH:mm'),null,null,85) 
    const loader = await this.loadingCtrl.create({duration :500});
    loader.present();    
    this.service.addTrainning("trainings",training).subscribe((data :Training )=>{
   
      if(data)
      { 
      
        loader.onWillDismiss().then(() => {
          this.presentAlertConfirm('Confirmation!',"Nouvelle séance d'entrainement ajoutée")
         });
         this.affiche = false
         this.service.trainings.push(data)
      }
      else
      {
        loader.onWillDismiss().then(() => {
          this.presentAlertConfirm('Probléme!',"La séance n'est pas ajoutée")
         });
      }
    
    });  
   
  } 
  
  async presentAlertConfirm(header,message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: header,
      message: message ,
      buttons: [
 {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }



  getTrainings()
{
  this.navCtrl.navigateForward("trainings")   
}
 
  
  async endTraining()
   {
    const loader = await this.loadingCtrl.create({duration :500});
    loader.present();
    var training  = this.service.trainings[this.service.trainings.length-1]
    training.duration = this.tempsEcoule(this.service.trainings[this.service.trainings.length-1].startTime,new DatePipe("fr-FR").transform(Date.now(),'yyyy-MM-dd HH:mm'))
    training.exercise = new Exercise(this.exercice.id)
    //provisoire
    training.startTime= training.startTime.replace('T',' ').substring(0,16)
    this.service.modifyTraining("trainings",training ).subscribe((data : Training)=>{

       if(data!= undefined)
       { 
        this.service.trainings[this.service.trainings.length-1].duration = data.duration 
        loader.onWillDismiss().then(() => {
           this.presentAlertConfirm('Confirmation!',"La séance est terminée")
          });  
          this.affiche = true
       }
       else
       {
         loader.onWillDismiss().then(() => {
           this.presentAlertConfirm('Probléme!',"Modification non effectuée")
          });
          this.service.trainings[this.service.trainings.length-1].duration=85
       }
     
     });  
   }


  back()
  {
    this.navCtrl.navigateBack("exercices")
  }

  security(src)
  {
    return this._sanitizationService.bypassSecurityTrustResourceUrl(src);
  }

  ionViewDidLeave()
  {
    this.affiche=null
  }

}
