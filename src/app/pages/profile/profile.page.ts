import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController, ToastController, MenuController, AlertController, ActionSheetController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { WebView } from '@ionic-native/ionic-webview';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  base64Image:any   
  user :User
  badges =["../../../assets/img/badge1.png","../../../assets/img/badge2.png","assets/img/badge3.png","assets/img/badge4.png","assets/img/badge5.png","assets/img/badge6.png"]

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private alertController: AlertController,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private transfer : FileTransfer,
    public actionSheetController: ActionSheetController,
    private service :CoachfootballService,
    public global : GlobalsService 
    ) {
      
     }

     ngOnInit() {
    }
  
     async ionViewWillEnter()
    {
    /*  this.user = await this.global.getUserProfil()
      this.base64Image =this.user.image = "assets/img/avatar.jpeg"
      this.menuCtrl.enable(true) */
    }

    pickImage(sourceType) {
      const options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        saveToPhotoAlbum: true,
        correctOrientation: true,
      }
      this.camera.getPicture(options).then((imageData) => {
     
       this.base64Image =  WebView.convertFileSrc(imageData);
        const fileTransfer: FileTransferObject = this.transfer.create();
  
        let options1: FileUploadOptions = {
           fileKey: 'file',
           fileName: this.base64Image+'.jpg',
           headers: {}
        }
    
    fileTransfer.upload(imageData,/* baseUrl+*/ '/upload' , options1)
     .then((data) => {
       // success
       alert("success");
     }, (err) => {
       // error
       alert("error"+JSON.stringify(err));
     });
       
      }, (err) => {
       alert('probléme de téléchargement image')
      });
    }
  
    editProfile()
    {
      this.navCtrl.navigateRoot("settings")
    }
    async selectImage() {
      const actionSheet = await this.actionSheetController.create({
        header: "Photo de profil",
        buttons: [{
          text: 'Choisir une image de galerie',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'prendre une photo',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Annuler',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }

}
