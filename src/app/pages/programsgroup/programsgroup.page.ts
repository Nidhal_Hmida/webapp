import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, MenuController, NavController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { ProgramGroup } from '../../models/programgroup.model';
import { CoachfootballService } from '../../services/coachfootballservice.service';
import { GlobalsService } from '../../services/globals/globals.service';

@Component({
  selector: 'app-programsgroup',
  templateUrl: './programsgroup.page.html',
  styleUrls: ['./programsgroup.page.scss'],
})

export class ProgramsgroupPage implements OnInit {
   
  
   programgroups : ProgramGroup [] = []
 
  user : User

  async ionViewWillEnter() {

    this.menuCtrl.enable(true);
 
  }
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private service :CoachfootballService,
    public global : GlobalsService,
    public router : Router
   
  ) {
    
   }
  
   ngOnInit() {
    this.programgroups = this.service.programsgroups
    
  }

  getPrograms(program:ProgramGroup)
  {
     this.service.getChild("/program_groups",program.id,"programs").subscribe((data: ProgramGroup [])=>{  
    if(data)
    {
     
       this.router.navigate(["/programs1"])
       this.service.servicePrograms(data)
    }
         });
    }
    


}
