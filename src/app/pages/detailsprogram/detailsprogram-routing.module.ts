import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsprogramPage } from './detailsprogram.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsprogramPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsprogramPageRoutingModule {}
