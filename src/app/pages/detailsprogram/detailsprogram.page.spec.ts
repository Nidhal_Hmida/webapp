import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsprogramPage } from './detailsprogram.page';

describe('DetailsprogramPage', () => {
  let component: DetailsprogramPage;
  let fixture: ComponentFixture<DetailsprogramPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsprogramPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsprogramPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
