import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsprogramPageRoutingModule } from './detailsprogram-routing.module';

import { DetailsprogramPage } from './detailsprogram.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsprogramPageRoutingModule
  ],
  declarations: [DetailsprogramPage]
})
export class DetailsprogramPageModule {}
