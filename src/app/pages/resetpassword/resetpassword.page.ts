import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController, ActionSheetController, AlertController } from '@ionic/angular';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {

  public onForm: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye';
  passwordType1: string = 'password';
  passwordIcon1: string = 'eye'; 

  constructor(  private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private alertController: AlertController,
    public loadingCtrl: LoadingController,
    public actionSheetController: ActionSheetController,
    private service :CoachfootballService,) { 
   
  }

  ngOnInit(): void {
    this.onForm = this.formBuilder.group({
      'pass': [null,Validators.compose([
        Validators.required,
        Validators.minLength(8),
        this.whiteSpace
       
      ])],
      'passConfirm': [null,Validators.compose([
        Validators.required,
        Validators.minLength(8),
        this.whiteSpace
      ])],
      'jeton': [null,Validators.compose([
        Validators.required,
        
      ])],
     
    },{validator:this.pwdMatchValidator});
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye' ? 'eye-off' : 'eye';
                    }

  hideShowPassword1() {
                      this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
                      this.passwordIcon1 = this.passwordIcon1 === 'eye' ? 'eye-off' : 'eye';
                  }
  
  pwdMatchValidator(frm: FormGroup) {
    return frm.get('pass').value === frm.get('passConfirm').value
       ? null : {'mismatch': true};
  }

  whiteSpace(control: FormControl) {
   
    return JSON.stringify(control.value).indexOf(' ') == -1
       ? null : {'whitespace': true};
  
  }
  whiteSpaceConfirmation(frm: FormGroup) {
    return frm.get('passConfirm').value.indexOf('') != -1
       ? null : {'whitespaceConfirm': true};
  }



  async presentAlertConfirm(titre,message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: titre,
      message: message,
      buttons: [
 {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }

  back()
  {
    this.navCtrl.navigateBack('emailrestpassword')
  }
   resetPassword()
   {
    if(this.onForm.valid)
    {
    
    }
    else
    {
      if(!this.onForm.get("pass").valid || !this.onForm.get("passConfirm").valid)
     this.presentAlertConfirm("Mot de passe non valide","<b>Ton mot de passe n'est pas valide <br>La mot de passe doit comporter  au moin 8 caractéres et ne doit pas contenir d'espaces <br><br>Vérifie ton mot de passe et réssaye</b>")
   
    else if(!this.onForm.get("jeton").valid)
    {
      this.presentAlertConfirm("Jeton non valide","<b>Ton jeton n'est pas valide <br><br>Vous devez saisir le jeton envoyé à votre addresse e-mail pour réinitialisez le mot de passe</b>")
    }
  }
   }

}
