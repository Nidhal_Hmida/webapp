import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailstrainingPage } from './detailstraining.page';

const routes: Routes = [
  {
    path: '',
    component: DetailstrainingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailstrainingPageRoutingModule {}
