import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailstrainingPage } from './detailstraining.page';

describe('DetailstrainingPage', () => {
  let component: DetailstrainingPage;
  let fixture: ComponentFixture<DetailstrainingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailstrainingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailstrainingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
