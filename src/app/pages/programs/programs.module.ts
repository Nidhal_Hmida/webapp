import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProgramsPageRoutingModule } from './programs-routing.module';
import { ProgramsPage } from './programs.page';

@NgModule({
  imports: [
    SharedModule,
    ProgramsPageRoutingModule
  ],
  declarations: [ProgramsPage]
})
export class ProgramsPageModule {}
