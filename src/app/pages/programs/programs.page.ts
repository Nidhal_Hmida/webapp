import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, MenuController, ToastController, NavController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { Program } from '../../models/program.model';
import { ProgramGroup } from '../../models/programgroup.model';
import { CoachfootballService } from '../../services/coachfootballservice.service';
import { GlobalsService } from '../../services/globals/globals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.page.html',
  styleUrls: ['./programs.page.scss'],
})

export class ProgramsPage implements OnInit {

   
   programs : Program[]
   programgroups1 : ProgramGroup [] = [{"id":"1","title":"Tirs","photo":"../../../assets/img/penaltie.jpg"},{"id":"2","title":"Passes","photo":"../../../assets/img/passes.jpg"},{"id":"3","title":"Vitesse","photo":"../../../assets/img/vitesse.jpg"},{"id":"4","title":"Dribbles","photo":"../../../assets/img/dribble.jpg"},
  {"id":"5","title":"Coordination","photo":"../../../assets/img/coordination.jpg"},{"id":"6","title":"Juggling","photo":"../../../assets/img/jj.jpg"},{"id":"7","title":"physique","photo":"../../../assets/img/physique.jpg"},{"id":"8","title":"Panna","photo":"../../../assets/img/panna.jpg"}
 ]
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private alertController: AlertController,
    private service :CoachfootballService,
    public global : GlobalsService,
    private router: Router
  ) {
    
   }

   image()
  {
    for(var i=0;i<this.programs.length;i++)
    {
      this.programs[i]["photo"] = this.programgroups1[i]["photo"] 
    }
  }

  ngOnInit() {
   
  }


  ionViewWillEnter()
  {
    this.programs = this.service.programs
    this.image()
  }
  

  details(program : Program) {
    this.navCtrl.navigateForward("detailsprogram")
    this.service.setCurrentProgram(program)
    }

    back()
    {
      this.navCtrl.navigateBack('programs')
    }

    

}
