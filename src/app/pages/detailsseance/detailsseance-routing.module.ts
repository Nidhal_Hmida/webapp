import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsseancePage } from './detailsseance.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsseancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsseancePageRoutingModule {}
