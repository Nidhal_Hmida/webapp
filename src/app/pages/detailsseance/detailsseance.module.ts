import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsseancePageRoutingModule } from './detailsseance-routing.module';

import { DetailsseancePage } from './detailsseance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsseancePageRoutingModule
  ],
  declarations: [DetailsseancePage]
})
export class DetailsseancePageModule {}
