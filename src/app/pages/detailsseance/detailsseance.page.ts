import { Component, OnInit } from '@angular/core';
import {  MenuController, NavController } from '@ionic/angular';
import { Seance } from 'src/app/models/seance.model';
import { CoachfootballService } from '../../services/coachfootballservice.service';
import { GlobalsService } from '../../services/globals/globals.service';

@Component({
  selector: 'app-detailsseance',
  templateUrl: './detailsseance.page.html',
  styleUrls: ['./detailsseance.page.scss'],
})

export class DetailsseancePage implements OnInit {

  seance :Seance
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private service :CoachfootballService,
    public global : GlobalsService
  ) { }

 
  ionViewWillEnter()
  { 
    this.seance = this.service.currentSeance  
  }

ngOnInit() {
  this.seance = this.service.currentSeance    
}

getexercices()
{
   this.service .getChild("/seances",this.seance.id,"exercises").subscribe((data: Seance[])=>{  
    this.service.serviceExercices(data)  
    if(data)
    {
    this.navCtrl.navigateForward("/exercices")
    }
     });  
}
 

  back()
  {
    this.navCtrl.navigateBack("/seances")
  }

  

}
