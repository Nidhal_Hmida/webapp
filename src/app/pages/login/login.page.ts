import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye';

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    private formBuilder: FormBuilder,
    private service :CoachfootballService,
    public global : GlobalsService,
    private googlePlus: GooglePlus,
    private fb: Facebook
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      'pass': [null, Validators.compose([
        Validators.required,
        this.whiteSpace
      ])]
    });
  }

   forgotPass() {
    this.navCtrl.navigateForward("/emailrestpassword")
  }

  whiteSpace(control: FormControl) {
    if(control.touched)
    {
    return JSON.stringify(control.value).indexOf(' ') == -1
       ? null : {'whitespace': true};
  }
  }
  async signIn() {
    if(this.onLoginForm.valid)
    {
     /* const loader = await this.loadingCtrl.create({
      duration: 500
    });

    loader.present();
    var  user = new User(null,null,this.onLoginForm.get("email").value,this.onLoginForm.get("password").value,null,null,null)
    var response =this.service.singnIn(user)
    if(response != null)
   loader.onWillDismiss().then(() => {
    this.navCtrl.navigateRoot("/")
      this.global.setUserProfil(response)
     
    });
    else
    alert("Vous devez  s'inscrire ou vérifiez vos infomations de connexion")*/
    }
    else
    {
      
     if(!this.onLoginForm.get('email').valid)
     this.presentAlertConfirm('Adresse email non valide',"<b>Ton adresse e-mail n'est pas valide <br> <br>Essaye de vérifier ton adresse e-mail et réessaye<br></b>")
    else if(!this.onLoginForm.get("pass").valid )
    this.presentAlertConfirm("Mot de passe non valide","<b>Ton mot de passe n'est pas valide <br>La mot de passe doit comporter  au moin 8 caractéres et ne doit pas contenir d'espaces <br><br>Vérifie ton mot de passe et réssaye</b>")
    
  }
  
  }
  // // //
  goToRegister() {
    this.navCtrl.navigateForward('/register');
  }

  
  async doGoogleLogin(){
 /*   this.googlePlus.login({})
    .then(res =>{ 
      var user = new User(null,res["displayName"],res["email"],null,null,null,res["imageUrl"])
      this.service.serviceUser(user)
   this.goToRegister()
    } )
    .catch(err => alert("Probléme technique"));
  */
  }

  fbLogin() {
    
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if (res.status === 'connected') {
          this.getUserDetail(res.authResponse.userID);
        } else {
         
        }
      })
      .catch(e => alert('Error logging into Facebook'+ e));
  }
  
  getUserDetail(userid: any) {
  /*  this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
      .then(res => {
        var user = new User(null,res["name"],res["email"],null,null,null,res["picture"]["data"]["url"])
        this.service.serviceUser(user)
        this.goToRegister()
      })
      .catch(e => {
        alert("probléme technique"+e)
      }); */
  }

  
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye' ? 'eye-off' : 'eye';
                    }

                    async presentAlertConfirm(titre,message) {
                      const alert = await this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: titre,
                        message: message,
                        buttons: [
                   {
                            text: 'Ok',
                            handler: () => {
                            }
                          }
                        ]
                      });
                  
                      await alert.present();
                    }

}
