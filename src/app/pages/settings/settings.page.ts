import { Component, OnInit } from '@angular/core';
import { TranslationService } from 'src/app/services/translation.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController, ToastController, MenuController, AlertController, ActionSheetController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  lang: string;
  supp : string
  onUpdateForm: FormGroup;
  base64Image:any 
  passwordType: string = 'password';
  passwordIcon: string = 'eye';
  passwordType1: string = 'password';
  passwordIcon1: string = 'eye';  
  user :User
  countries = [{"name":"France","flag":"../../../assets/img/fr.png"},{"name":"Suisse","flag":"../../../assets/img/sui.png"},{"name":"Belgique","flag":"../../../assets/img/bel.png"},{"name":"Tunisie","flag":"../../../assets/img/tn.png"},{"name":"Algérie","flag":"../../../assets/img/alg.png"},{"name":"Maroc","flag":"../../../assets/img/mar.png"}]

  constructor(  public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    private formBuilder: FormBuilder,
    public actionSheetController: ActionSheetController,
    private service :CoachfootballService,
    public global : GlobalsService ,
    private translateConfigService: TranslationService,
    private menuCntrl : MenuController,
              )
     { }

    

  ngOnInit() {
    this.onUpdateForm = this.formBuilder.group({
      'fullName': [null, Validators.compose([
        Validators.required
      ])],
      'email': [null],
      'pass': [null, Validators.compose([
        Validators.required
      ])],
      'passConfirm': [null, Validators.compose([
        Validators.required
      ])],
      'country': [null, Validators.compose([
        Validators.required
      ])]
    });
    this.lang = this.translateConfigService.getDefaultLanguage()
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye' ? 'eye-off' : 'eye';
                    }

  hideShowPassword1() {
                      this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
                      this.passwordIcon1 = this.passwordIcon1 === 'eye' ? 'eye-off' : 'eye';
                  }

   async ionViewWillEnter()
  {
    
 /*   this.user = await this.global.getUserProfil()
    this.base64Image =this.user.image = "assets/img/avatar.jpeg"
    this.onUpdateForm.get("fullName").setValue(this.user.fullname)
    this.onUpdateForm.get("email").setValue(this.user.email)
    this.onUpdateForm.get("date").setValue(this.user.birthday)
    this.onUpdateForm.get("password").setValue(this.user.password)
   /* this.onUpdateForm.get("preferences").setValue(this.user.prefrences)*/
    this.menuCtrl.enable(false) 
    
  }

  back()
  {
    this.navCtrl.navigateBack("edit-profile")
  }
  loadFlags() {
    setTimeout(()=>{ 
     let radios=document.getElementsByClassName('alert-radio-label');
     for (let index = 0; index < radios.length; index++) {
        let element = radios[index];
        element.innerHTML=element.innerHTML.concat('<img class="country-image"  style="width: 20px;height:20px;float:left" src="'+this.countries[index].flag+'" />');
      }
  }, 500);
}

  async sendData() {
  /*  const loader = await this.loadingCtrl.create({
      duration: 2000
    });
    loader.present();
    var  user = new User(this.user.id,this.onUpdateForm.get("fullName").value,this.onUpdateForm.get("email").value,this.onUpdateForm.get("password").value,this.onUpdateForm.get("date").value,this.onUpdateForm.get("preferences").value,this.base64Image)
    this.service.updateUser(user)
    loader.onWillDismiss().then(async l => {
      this.global.setUserProfil(user)
      const toast = await this.toastController.create({
        cssClass: 'bg-profile',
        message: 'vos données sont modifiées!',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      this.navCtrl.navigateForward('/home-results');
    });*/
  }

 

  languageChanged(){
    this.translateConfigService.setLanguage(this.lang);
  }

}
