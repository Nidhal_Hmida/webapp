import { NgModule } from '@angular/core';
import { RegisterPage } from './register.page';
import { RegisterPageRoutingModule } from './register-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    RegisterPageRoutingModule,
  ],
  declarations: [RegisterPage,
  
  ]
})
export class RegisterPageModule {}
