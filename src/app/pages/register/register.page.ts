import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController, ActionSheetController, AlertController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { User } from 'src/app/models/user.model';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { ToastController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { WebView } from '@ionic-native/ionic-webview';

const baseUrl = "http://192.168.1.13:3000";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})

export class RegisterPage implements OnInit {
  
  onRegisterForm: FormGroup;
  onRegisterForm2: FormGroup;
  base64Image:any   ="assets/img/profil.jpg";
  user : User
  stepCondition = true
  passwordType: string = 'password';
  passwordIcon: string = 'eye';
  passwordType1: string = 'password';
  passwordIcon1: string = 'eye';
  countries = [{"name":"France","flag":"../../../assets/img/fr.png"},{"name":"Suisse","flag":"../../../assets/img/sui.png"},{"name":"Belgique","flag":"../../../assets/img/bel.png"},{"name":"Tunisie","flag":"../../../assets/img/tn.png"},{"name":"Algérie","flag":"../../../assets/img/alg.png"},{"name":"Maroc","flag":"../../../assets/img/mar.png"}]
  step =1
  
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private transfer : FileTransfer,
    public actionSheetController: ActionSheetController,
    private service :CoachfootballService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.onRegisterForm = this.formBuilder.group({
   
      'email': [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],  'pass': [null,Validators.compose([
        Validators.required,
        Validators.minLength(8),
       
      ])],
      'passConfirm': [null,Validators.compose([
        Validators.required,
        Validators.minLength(8),
       
      ])],
    },  { 
      validator: this.pwdMatchValidator
    });

    this.onRegisterForm2 = this.formBuilder.group({
      'fullName': [null, Validators.compose([
        Validators.required
      ])],
      
      'country': [null, Validators.compose([
        Validators.required
      ])],
    
    });
    
  }

  loadFlags() {
    setTimeout(()=>{ 
     let radios=document.getElementsByClassName('alert-radio-label');
     for (let index = 0; index < radios.length; index++) {
        let element = radios[index];
        element.innerHTML=element.innerHTML.concat('<img class="country-image"  style="width: 20px;height:20px;float:left" src="'+this.countries[index].flag+'" />');
      }
  }, 500);
}

  pwdMatchValidator(frm: FormGroup) {
    return frm.get('pass').value === frm.get('passConfirm').value
       ? null : {'mismatch': true};
  }
  
  getCountry(selectedCountryObject: any){
    console.log(selectedCountryObject)
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
   
      this.user = this.service.user
    if(this.user != undefined)
    {
      this.onRegisterForm.get("fullName").setValue(this.user.fullname)
      this.onRegisterForm.get("email").setValue(this.user.email)
      this.base64Image = this.user.image
    }
  }

 
  async signUp() {

    if(!this.onRegisterForm.get("country").valid)
      {
        this.presentAlertConfirm("Pays non valide","<b>Ton pays pays n'est pas valide<br><br>Choisie un pays et réessaye</b>")
      }

      else
      {
   /* const loader = await this.loadingCtrl.create({
      duration: 500
    });

    loader.present();
    var  user
    if(this.user == undefined)
   user = new User(""+Math.floor(Math.random()*Math.floor(9999)),this.onRegisterForm.get("fullName").value,this.onRegisterForm.get("email").value,this.onRegisterForm.get("password").value,this.onRegisterForm.get("date").value,this.onRegisterForm.get("preferences").value,this.base64Image)
    else
    {
    this.user.birthday =this.onRegisterForm.get("date").value
    this.user.prefrences = this.onRegisterForm.get("preferences").value
    alert(JSON.stringify(this.user))
    }
  var response =this.service.singnUp(user)
  
  
   
   loader.onWillDismiss().then(() => {
     this.presentAlertConfirm()
      this.navCtrl.navigateRoot('/login');
    });*/
  }
  }

  // // //
  goToLogin() {
    this.navCtrl.navigateRoot('/login');
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      correctOrientation: true,
    }
    this.camera.getPicture(options).then((imageData) => {
   
     this.base64Image =  WebView.convertFileSrc(imageData);
      const fileTransfer: FileTransferObject = this.transfer.create();

      let options1: FileUploadOptions = {
         fileKey: 'file',
         fileName: this.onRegisterForm.get("fullName").value+'.jpg',
         headers: {}
      }
  
  fileTransfer.upload(imageData, baseUrl+ '/upload' , options1)
   .then((data) => {
     // success
     alert("success");
   }, (err) => {
     // error
     alert("error"+JSON.stringify(err));
   });
     
    }, (err) => {
     alert('probléme de téléchargement image')
    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Photo de profil",
      buttons: [{
        text: 'Choisir une image de galerie',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'prendre une photo',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Annuler',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: 'Click to Close',
      position: 'top',
      buttons: [
        {
          side: 'start',
          icon: 'star',
          text: 'Favorite',
          handler: () => {
            console.log('Favorite clicked');
          }
        }, {
          text: 'Done',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  
  async presentAlertConfirm(titre,message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: titre,
      message: message,
      buttons: [
 {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }


  ionViewWillLeave()
  {
    this.service.serviceUser(undefined)
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye' ? 'eye-off' : 'eye';
                    }

  hideShowPassword1() {
                      this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
                      this.passwordIcon1 = this.passwordIcon1 === 'eye' ? 'eye-off' : 'eye';
                  }
                 
                  back() {
                    if(this.step!=1)
                    {
                    this.step--
                    }
                    else
                    {
                      this.navCtrl.navigateRoot("login")
                    }
                
                  }
                 
                  next() {
                   this.step++
                 
               /*
                    if(this.onRegisterForm.valid)
    {

     
    
    }
    else
    {
      if(!this.onRegisterForm.get("fullName").valid)
      {
        this.presentAlertConfirm("Nom non valide","<b>Ton nom n'est pas valide <br>Le prénom et le nom de famille ne peuvent pas etre vides.<br><br>Vérifie ton nom et réessaye</b>")
      }
     else  if(!this.onRegisterForm.get('email').valid)
     this.presentAlertConfirm('Adresse email non valide',"<b>Ton adresse e-mail n'est pas valide <br> <br>Essaye de vérifier ton adresse e-mail et réessaye<br></b>")
      else if(!this.onRegisterForm.get("pass").valid || !this.onRegisterForm.get("passConfirm").valid)
     this.presentAlertConfirm("Mot de passe non valide","<b>Ton mot de passe n'est pas valide <br>La mot de passe doit comporter  au moin 8 caractéres et ne doit pas contenir d'espaces <br><br>Vérifie ton mot de passe et réssaye</b>")
   
                  }*/
                }

}

