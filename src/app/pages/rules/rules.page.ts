import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController, ToastController, MenuController, AlertController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.page.html',
  styleUrls: ['./rules.page.scss'],
})

export class RulesPage implements OnInit {
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
  
  ) { }

  ngOnInit() {
  }

  back()
  {
    this.navCtrl.navigateBack('programs')
  }

}
