import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, MenuController, ToastController, NavController } from '@ionic/angular';
import { Seance } from '../../models/seance.model';
import { CoachfootballService } from '../../services/coachfootballservice.service';
import { GlobalsService } from '../../services/globals/globals.service';

@Component({
  selector: 'app-seance',
  templateUrl: './seance.page.html',
  styleUrls: ['./seance.page.scss'],
})

export class SeancePage implements OnInit {
  
  seances : Seance[]
  programgroups1  = [{"id":"1","title":"Tirs","photo":"../../../assets/img/penaltie.jpg"},{"id":"2","title":"Passes","photo":"../../../assets/img/passes.jpg"},{"id":"3","title":"Vitesse","photo":"../../../assets/img/vitesse.jpg"},{"id":"4","title":"Dribbles","photo":"../../../assets/img/dribble.jpg"},
  {"id":"5","title":"Coordination","photo":"../../../assets/img/coordination.jpg"},{"id":"6","title":"Juggling","photo":"../../../assets/img/jj.jpg"},{"id":"7","title":"physique","photo":"../../../assets/img/physique.jpg"},{"id":"8","title":"Panna","photo":"../../../assets/img/panna.jpg"}
 ]
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
    private service :CoachfootballService,
    public global : GlobalsService
  ) { }

  
   ionViewWillEnter()
   {
     this.menuCtrl.enable(false);
     this.seances = this.service.seances
     this.image()
   }


  image()
   {
     for(var i=0;i<this.seances.length;i++)
     {
       this.seances[i]["photo"] = this.programgroups1[i]["photo"] 
     }
   }

  
  details(seance : Seance) {
    this.service.setCurrentSeance(seance)
    this.navCtrl.navigateRoot("detailsseance")
    }

    back() {
      console.log("hello")
      this.navCtrl.navigateBack("detailsprogram")
      }
  

  ngOnInit() {}

}
